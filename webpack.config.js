const path = require("path");
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
module.exports = function(env) { 
    let prod = env !== undefined && env.production === true; 
    let dev = env !== undefined && env.development === true; 
    
return {
    devtool: prod ? "source-map" : "cheap-module-eval-source-map",
    plugins: [
        new MiniCssExtractPlugin({
        filename: prod ? "[name].[chunkhash].css": "[name].css",
        chunkFilename: '[id].css',
        }),
        new HtmlWebpackPlugin({
            title: 'Portfolio',
            template: './src/index.html'
          })

    ],
    mode: 'none',
    entry:{
      app:"./src/js/app.js"  
    },
    output:{
        publicPath: dev ? "/dist/": "",
        path:path.resolve(__dirname, "dist/"),  
        filename: prod ? "[name].[chunkhash].js": "[name].js"
    },

    module: {
        rules: [
          {
            test: /\.m?js$/,
            exclude: /node_modules/,
            use: {
              loader: 'babel-loader',
              options: {
                presets: ['@babel/preset-env']
              }
            }
          },
          {
            test: /\.css$/i,
            use: [{
                loader:MiniCssExtractPlugin.loader,
                    options: {
                     url: true,
                    },
                },
                'css-loader',],
                
          },
          {
            test: /\.png$/i,
            use: 'raw-loader',
          },
          {
            test: /\.(png|jpe?g|gif)$/i,
            loader: 'file-loader',
          options: {
          name: '[name].[ext]',
        },
          },
        ],
      },
    }
}
import observable from './observable'
import { prototype } from 'events';

const Language = (function(){
    function Language(Ref, languages, defaultLan = 'pl'){
        this.Ref = Ref;
        this.lans = languages;
        this.lan = defaultLan;
        this.obseverLan = null;
    }
    Language.prototype.putOnPeage = function(){
        const htmlReady =  this.create()               
        this.Ref.appendChild(htmlReady);
    }
    Language.prototype.create = function(){
        return this.lans.reduce((docFragment, lan) => {
            const input = this.input(lan);
            const label = this.label(lan);
            label.appendChild(input);
            docFragment.appendChild(label);
            return docFragment;
        },new DocumentFragment())
    }
    Language.prototype.input = function(lan){
        const input = document.createElement('input');
        input.value = lan;
        input.type = 'radio';
        input.name = 'language';
        input.checked = lan === this.lan ? true : false;
        input.onchange = () => this.chengeLan(lan);
        return input;
    }
    Language.prototype.label = function(lan){
        const label = document.createElement('label');
        label.textContent = lan;
        return label;
    }
    Language.prototype.chengeLan = function(lan){
        this.lan = lan;
        return this.obseverLan ? this.obseverLan(lan) : null;
    }
    Language.prototype.getObsarvable = function(observable){
        this.obseverLan = observable ?  observable(this.lan) : null;
        return this.obseverLan;
        
    }
    return Language;
    })();

const lan = new Language(document.querySelector("#language"), ["pl","en"]);
lan.putOnPeage();
export default lan.getObsarvable(observable);

 


import Html from './HTML/HTMLprojects';
// import observableLanguage from './language';
import projects from './fetch-projects';


const Projects = (function(){
   
    function Projects(projects){
        this.projects = projects;
        this.start()
    }
    Projects.prototype.start = function(){
        this.projects.then((projects)=>{
            const HTML = this.iteratio(projects);
            Html.appendAll(HTML);
        })
    }
    Projects.prototype.iteratio = function(projects){
        return projects.map(project =>{
            return ''.concat(...this.invokeStack(project))
       });
    }
    Projects.prototype.invokeStack = function(project){
        return Html.callStack.map((stack =>{
            return  Html.assignProp(this.glue(project, stack.properties),stack.nameObj)
        }));
    }
    Projects.prototype.glue = function(project, properties){
        return properties.map(key => { return {[key]:project[key]}
        }); 
    }  
    return Projects;
 })();
export  default new Projects(projects);

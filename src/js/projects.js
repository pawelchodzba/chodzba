
 const StaticText = function() {
     return {
        invitation:{
            en:`<p>My name is Paweł Chodźba </p><p>I have been passinate about programming for a number of year. This website covers my completed projects</p>
            <p>J have made video presentations of some of the aplication. I invite you to have a look (Polish lector)</p>`,
            pl:`<p>Nazywam się Paweł Chodźba </p><p> Od kilku lat pasjonuję się programowaniem. Niniejsza witryna prezentuje wykonane projekty.</p>
                <p>Do niektórych aplikacji utworzyłem krótkie prezentacje video. Zapraszam do oglądania.</p>`
        },
        makeDate:{
            en:'creation date',
            pl:'data utworzenia'
        },
        clientName:{
            en:'customer name',
            pl:'nazwa klienta'
        },
        technology:{
            en:'technologies',
            pl:'technologie'
        },
        linkName:{
            apllication:{
                en:'aplication',
                pl:'aplikacja'
            },
            code:{
                en:'code on gitHub',
                pl:'kod na gitHub'
            },
            videoPresentation:{
                en:'video',
                pl:'film'
            }
           
        },
        credential:{
            en:['login', 'password'],
            pl:['login', 'hasło']
        },
        linkProject:{
            en:'link to the project code',
            pl:'link do kodu projektu'
        }

     }
 }

 const staticText = StaticText();
 export  {staticText};
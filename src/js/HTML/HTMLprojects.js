import {staticText as txt} from '../projects';
import observableLanguage from '../language';
import controll from '../controll';
import head from './head';
import body from './body';
import footer from './footer';
import maineFooter from './maine-footer';

const HTML = (function(ObjHtml){

    function Html(RefHTML){ 
        this.callStack = [
            {
                nameObj:'head',
                properties:['title', 'date', 'client']
            },
            {
                nameObj:'body',
                properties:['description','status','credential']
            },
            {
                nameObj:'footer',
                properties:['links','technology']
            }
        ];
        this.RefHTML = RefHTML;
        this.language = '';
    }
    Html.prototype.setLanguage = function(observableLanguage){ 
        this.language = observableLanguage();
        observableLanguage.subscriber((lan)=>{
            this.language = lan;
            controll.start();
        })
    }
    Html.prototype.assignProp = function(properties, nameObj){
        return ObjHtml[nameObj].html(properties);
    }
    Html.prototype.appendAll = function(HTML){
        let html = `<div class = project>${HTML.join("</div> <div class = project>")}</div>`;
        this.RefHTML.projects.innerHTML = html;
        this.RefHTML.invitation.innerHTML = txt.invitation[this.language];
        this.RefHTML.footer.innerHTML = maineFooter.html();
    }
    return Html;
    }
)({
    head:head,
    body:body,
    footer:footer
    });
    const Html = new HTML({
        projects:document.querySelector('#projects'),
        invitation:document.querySelector('#invitation'),
        footer:document.querySelector('#footer'),
    });
    Html.setLanguage(observableLanguage);
    export default Html;
import Html from './HTMLprojects';
import {staticText as txt} from '../projects';
const Head = (function(){
  function Head(){}
    Head.prototype.html = function(properties){
      let [{title},{date},{client}] = properties;   
          return `<header>
                      <div>
                          <div>${title[Html.language]}</div>
                      </div>    
                      <div>
                          <div>
                              <p>${txt.clientName[Html.language]}:</p>
                              <p>${client[Html.language]}</p>
                          </div>  
                          <div>  
                              <p>${txt.makeDate[Html.language]}:</p>
                              <p>${date}</p>
                          </div>   
                      </div>
                  </header> `;
      }
    
 return Head;
 })();

 
 
  const head = new Head();


 export default head;
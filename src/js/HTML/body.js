import Html from './HTMLprojects';
import {staticText as txt} from '../projects';
const Body = (function(){
  function Body(){}
    Body.prototype.html = function(properties){
        let [{description},{status},{credential}] = properties;
        console.log()

        return `<article>
                        <div>${description[Html.language]}<div>
                        <div>${status[Html.language]}<div>
                        ${(credential&&credential.login)?`
                          <div class ="credential">
                            <div>${txt.credential[Html.language][0]}: ${credential.login}</div>
                            <div>${txt.credential[Html.language][1]}: ${credential.password}</div>
                          </div>
                          `:``} 
                </article>`;
      }
    
 return Body;
 })();

 
 
  const body = new Body();
 export default body;
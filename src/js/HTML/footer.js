import Html from './HTMLprojects';
import {staticText as txt} from '../projects';
const Footer = (function(){
  function Footer(){}
    Footer.prototype.html = function(properties){
        let {links} = properties[0];
        let {technology} = properties[1];
            return `<footer>
                        <p>${txt.technology[Html.language]}:</p>
                        <div>
                            ${(technology.backEnd)?`
                            <p>back-end:<br> ${technology.backEnd}</p>`:''}    
                            ${(technology.frondEnd)?`
                            <p>frond-end:<br> ${technology.frondEnd}</p>`:''}    
                           
                        </div>
                        <nav>    
                            ${(links.app)?`<a href = ${links.app}>${txt.linkName.apllication[Html.language]}</a>`:``} 
                            ${(links.code)?`<a href = ${links.code}>${txt.linkName.code[Html.language]}</a>`:``} 
                            ${(links.video)?`<a href = ${links.video}>${txt.linkName.videoPresentation[Html.language]}</a>`:``} 
                        </nav>
                    </footer>`;
      }
    
 return Footer;
 })();

 
 
  const footer = new Footer();
 export default footer;
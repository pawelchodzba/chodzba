
function observable(value) {
    const subscribers = [];
    function notify(newValue) {
        subscribers.forEach(subscriber => {
            subscriber(newValue);
        });
    }
    function accessor(newValue) {
        if (arguments.length && newValue !== value) {
            value = newValue;
            notify(newValue);
        }
        return value;
    }
    accessor.subscriber = function(subscriber) {
        subscribers.push(subscriber)
    }
    return accessor
}
export default observable;